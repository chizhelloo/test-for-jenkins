FROM openjdk:11-jdk-slim AS build

WORKDIR /build

COPY . .

COPY --from=build /build/target/* ./
ENTRYPOINT [ "./mvnw", "spring-boot:run" ]
#ENTRYPOINT ["java", "-jar", "*.jar"]   
